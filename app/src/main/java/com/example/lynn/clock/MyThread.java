package com.example.lynn.clock;

import java.util.Calendar;

import static com.example.lynn.clock.MainActivity.*;

/**
 * Created by lynn on 6/8/2016.
 */
public class MyThread implements Runnable {
    private Thread thread;
    private boolean keepGoing;

    public MyThread() {
        thread = new Thread(this);

        keepGoing = true;

        thread.start();
    }

    public void pause(double seconds) {
        try {
            Thread.sleep((int)(seconds*1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    public void getTime() {
        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);

        boolean am = hour < 12;

        hour -= (hour > 12) ? 12 : 0;

    }

    @Override
    public void run() {

    }
}
