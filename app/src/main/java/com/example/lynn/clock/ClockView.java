package com.example.lynn.clock;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import static com.example.lynn.clock.MainActivity.*;

/**
 * Created by lynn on 6/8/2016.
 */
public class ClockView extends LinearLayout {

    public ClockView(Context context) {
        super(context);

        digits = new TextView[9];

        for (int counter=0;counter<digits.length;counter++) {
            digits[counter] = new TextView(context);

            addView(digits[counter]);
        }

        digits[2].setText(":");
        digits[5].setText(":");
    }

}
